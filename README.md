# Pheno4HEP

This repository is for PHENO study in high energy physics, including generators, hadronizations, simulations and analysis tools.

## Generator

### POWHEG BOX

#### Setup

official website : https://powhegbox.mib.infn.it/

1,download the newest POWHEG-BOX-V2
```
svn checkout --username anonymous --password anonymous svn://powhegbox.mib.infn.it/trunk/POWHEG-BOX-V2
```
2,list the existing processes in the package
```
svn list --username anonymous --password anonymous svn://powhegbox.mib.infn.it/trunk/User-Processes-V2
```
3,download the desired process,e.g. VBF\_H
```
svn co --username anonymous --password anonymous svn://powhegbox.mib.infn.it/trunk/User-Processes-V2/VBF_H
```
4,set the `PATH` and `LIBRARY` of `LHAPDF` and `fastjet`. I use the library from `CMSSW`. You can also setup a standalone version.
```
#setup LHAPDF PATH
PATH=$PATH:/cvmfs/cms.cern.ch/slc6_amd64_gcc630/external/lhapdf/6.2.1-fmblme/bin/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/cms.cern.ch/slc6_amd64_gcc630/external/lhapdf/6.2.1-fmblme/bin/
#setup fastjet PATH
PATH=$PATH:/cvmfs/cms.cern.ch/slc6_amd64_gcc630/external/lhapdf/6.2.1-fmblme/bin/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/cms.cern.ch/slc6_amd64_gcc630/external/lhapdf/6.2.1-fmblme/bin/
```

5,compile. I use `gcc 10.3.0` from `CMSSW_12_3_0`. Please add one line in Makefile
```
FFLAGS=-fallow-argument-mismatch
```
after
```
ifeq ("$(COMPILER)","gfortran")
```

and then compile
```
cd VBF_H
make pwhg_main
```

#### Example

##### VBF\_H

1. inspect the file `powheg.input`

The first parts indicate the number of events, incoming particle and beam energy

```
numevts 100000    ! number of events to be generated
ih1   1           ! hadron 1 (1 for protons, -1 for antiprotons)
ih2   1           ! hadron 2 (1 for protons, -1 for antiprotons)
ebeam1 7000d0     ! energy of beam 1
ebeam2 7000d0     ! energy of beam 2
```

The following part indicates the parton distribution function.

```
lhans1  90400         ! pdf set for hadron 1 (LHA numbering)
lhans2  90400         ! pdf set for hadron 2 (LHA numbering)
```
You can see https://lhapdf.hepforge.org/ for more details of `LHAPDF`.


The following part indicates the Higgs information

```
hmass    125d0       ! Higgs boson mass
hwidth   4.14d-03    ! Higgs boson width

#higgsfixedwidth  1  ! if 1, use old behaviour with fixed width Breit-Wigner
                     ! default is running width

hdecaymode -1   ! -1 no decay
                ! 0 all decay channels open
                ! 1-6 d dbar, u ubar,..., t tbar
                ! 7-9 e+ e-, mu+ mu-, tau+ tau-
                ! 10  W+W-
                ! 11  ZZ
                ! 12  gamma gamma
```

The following part indicates how the calculation is performed.
```
use-old-grid    1 ! if 1 use old grid if file pwggrids.dat is present (<> 1 regenerate)
use-old-ubound  1 ! if 1 use norm of upper bounding function stored in pwgubound.dat, if present; <> 1 regenerate

ncall1 20000   ! number of calls for initializing the integration grid
itmx1    2     ! number of iterations for initializing the integration grid
ncall2 20000   ! number of calls for computing the integral and finding upper bounding envelope
itmx2    3     ! number of iterations for computing the integral and finding upper bouding envelope
foldcsi   1    ! number of folds on csi integration
foldy     1    ! number of folds on  y  integration
foldphi   1    ! number of folds on phi integration
nubound 20000 ! number of calls to set up the upper bounding norms for radiation

```

Other otpions
```
withnegweights 1  ! (default 0) if 1, use negative weights
xupbound 2d0   ! increase upper bound for radiation generation
#renscfact  1d0   ! (default 1d0) ren scale factor: muren  = muref * renscfact
#facscfact  1d0   ! (default 1d0) fac scale factor: mufact = muref * facscfact
#ptsupp     0d0   ! (default 0d0)  mass param for Born suppression factor (generation cut) If < 0 suppfact = 1
#bornonly   1      ! (default 0) if 1 do Born only
withdamp    1      ! (default 0, do not use) use Born-zero damping factor
#testplots 1

#iseed    5437     !  Start the random number generator with seed iseed
#rand1     0       !  skipping  rand2*100000000+rand1 numbers.
#rand2     0       !  (see RM48 short writeup in CERNLIB)

manyseeds 1       ! Used to perform multiple runs with different random
                   ! seeds in the same directory.
                   ! If set to 1, the program asks for an integer j.
                   ! The file pwgseeds.dat at line j is read, and the
                   ! integer at line j is used to initialize the random
                   ! sequence for the generation of the events.
                   ! The event file is called pwgevents-'j'.lhe

```

2. a


